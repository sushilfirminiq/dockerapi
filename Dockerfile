# Dockerfile for awscli.
FROM ubuntu:16.04

MAINTAINER Sushil

RUN apt-get update && \
    apt-get upgrade -y
# ---------------------------
# --- Install required tools
# ---------------------------
RUN apt-get install -y --no-install-recommends \
    zip \
    unzip \
    curl \
    python-pip \
    groff \
    python-setuptools \
	git

RUN	curl -sL https://deb.nodesource.com/setup_8.x | bash

RUN apt-get install -y nodejs && \
    apt-get clean

# ---------------------------
# --- Installing awscli
# ---------------------------
RUN pip install -U pip
RUN pip install awscli
RUN npm install -g pm2